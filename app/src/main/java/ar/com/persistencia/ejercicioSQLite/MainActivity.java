package ar.com.persistencia.ejercicioSQLite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import ar.com.persistencia.ejercicioSQLite.entidades.Producto;
import ar.com.persistencia.proyecto019.R;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etCodigo, etDescripcion, etPrecio;

    Button btnAlta;
    Button btnConsultaPorCodigo;
    Button btnConsultaPorDescripcion;
    Button btnBajaPorCodigo;
    Button btnModificacion;

    ProductoDAO productoDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Conexion con la base de datos
        productoDAO = new ProductoDAO(this);
        //Anclaje elementos visuales
        etCodigo =(EditText)findViewById(R.id.etCodigo);
        etDescripcion =(EditText)findViewById(R.id.etDescripcion);
        etPrecio =(EditText)findViewById(R.id.etPrecio);

        btnAlta = findViewById(R.id.btnAlta);
        btnConsultaPorCodigo = findViewById(R.id.btnConsultaPorCodigo);
        btnConsultaPorDescripcion = findViewById(R.id.btnConsultaPorDescripcion);
        btnBajaPorCodigo = findViewById(R.id.btnBajaPorCodigo);
        btnModificacion = findViewById(R.id.btnModificacion);

        //Listener de los botones
        btnAlta.setOnClickListener(this);
        btnConsultaPorCodigo.setOnClickListener(this);
        btnConsultaPorDescripcion.setOnClickListener(this);
        btnBajaPorCodigo.setOnClickListener(this);
        btnModificacion.setOnClickListener(this);
    }

    //Creacion del menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    //Accion a relizar en funcion de la opcion seleccionada del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /**
     * Inserta producto en la base de datos a partir de informacion proporcionada por el usuario,
     * lanza excepcion si ocurre error durante la insercion, si no ha rellenado todos los campos se
     * informa al usuario con mensaje toast.
     * La insercion se lleva cabo en un nuevo hilo, para no interferir con el UIThread
     */
    private void alta() {
        if( comprobarCampos(Arrays.asList(etCodigo,etPrecio,etPrecio)) ){

            new Thread(() -> {

                int codigo = Integer.parseInt(etCodigo.getText().toString());
                double precio = Double.parseDouble( etPrecio.getText().toString() );
                String descripcion = etDescripcion.getText().toString();
                Producto producto = new Producto(codigo,descripcion,precio);
                try{
                    productoDAO.insertarProducto(producto);
                    runOnUiThread(()->{
                        mensajeToast("Producto insertado en la base de datos");
                        limpiarCampos();
                    });
                }catch (SQLException sqlException){
                    runOnUiThread(()-> mensajeToast("Error al insertar el producto en la base de datos"));
                }

            }).start();

        }else{
            mensajeToast("Debe rellenar todos los campos para insertar el producto");
        }
    }

    /**
     * Consulta por codigo un producto en la base de datos y muestra el resultado en los
     * campos editables, asi es mas facil editarlos o copiar su valores, si no lo encuentra
     * informa al usario por Toast y limpia los campos.
     * La consulta se realiza en un hilo nuevo para no interferir con el the UI
     */
    private void consultarPorCodigo() {

        if( comprobarCampos(Arrays.asList(etCodigo)) ){

            new Thread(()->{
                int codigo = Integer.parseInt( etCodigo.getText().toString() );
                Producto producto = productoDAO.productoPorCodigo(codigo);
                if(producto != null){
                    runOnUiThread(()->{
                        etDescripcion.setText(producto.getDescripcion());
                        etPrecio.setText( Double.toString(producto.getPrecio()) );
                    });
                }else{
                    runOnUiThread(()->{
                        mensajeToast("No se econtro producto con codigo "+codigo);
                        limpiarCampos();
                    });
                }
            }).start();
        }else{
            mensajeToast("debe rellenar el campo codigo");
        }
    }

    /**
     * Consulta por descripcion un producto en la base de datos y muestra el resultado en los
     * campos editables, asi es mas facil editarlos o copiar su valores, si no lo encuentra
     * informa al usario por Toast y limpia los campos.
     * La consulta se realiza en un hilo nuevo para no interferir con el the UI
     */
    private void consultarPorDescripcion() {

        if( comprobarCampos(Arrays.asList(etDescripcion)) ){

            new Thread(()->{
                String descripcion = etDescripcion.getText().toString();
                Producto producto = productoDAO.productoPorDescripcion(descripcion);
                if( producto != null ){
                    runOnUiThread(()->{
                        etPrecio.setText( Double.toString(producto.getPrecio()) );
                        etCodigo.setText( Integer.toString(producto.getCondigo()) );
                    });
                }else{
                    runOnUiThread(()->{
                        mensajeToast("No encontrado producto con descripcion "+descripcion);
                        limpiarCampos();
                    });
                }
            }).start();
        }else{
            mensajeToast("debe completar el campo descripcion");
        }
    }

    /**
     * Borra de la base de datos el producto con el codigo especificado por el usuario en
     * el campo editable, se realiza en un hilo secundario
     */
    private void bajaPorCodigo() {

        if ( comprobarCampos(Arrays.asList(etCodigo)) ){

            new Thread(()->{
                int codigo = Integer.parseInt(etCodigo.getText().toString());
                String mensaje = productoDAO.eliminarPorCodigo(codigo) ?
                        "Producto BORRADO con exito" :
                        "No se pudo borrar el producto con codigo "+codigo;

                runOnUiThread(()->{
                    mensajeToast(mensaje);
                    limpiarCampos();
                });
            }).start();
        }else {
            mensajeToast("debe rellenar el campo codigo");
        }
    }

    /**
     * Modifica los campos descripcion y precio del producto con el codigo especificado
     * la tarea se realiza en un nuevo hilo de ejecucion
     */
    private void modificar() {

        if( comprobarCampos(Arrays.asList(etCodigo,etPrecio,etDescripcion)) ){

            new Thread(()->{

                int codigoProducto = Integer.parseInt(etCodigo.getText().toString());
                double precioProducto = Double.parseDouble(etPrecio.getText().toString());
                String descripcionProducto = etDescripcion.getText().toString();
                Producto producto = new Producto(codigoProducto,descripcionProducto,precioProducto);

                String mensaje = productoDAO.modificarProducto(producto) ?
                        "Producto modificado con exito" :
                        "NO se puede modificar el producto con codigo "+producto.getCondigo();
                runOnUiThread(()->{
                    mensajeToast(mensaje);
                    limpiarCampos();
                });
            }).start();
        }else {
            mensajeToast("Debe rellenar todos los campos para modificar un producto");
        }
    }

    @Override
    public void onClick(View click) {

        if( click.equals(btnAlta) )
            alta();

        if( click.equals(btnConsultaPorCodigo) )
            consultarPorCodigo();

        if( click.equals(btnConsultaPorDescripcion) )
            consultarPorDescripcion();

        if( click.equals(btnBajaPorCodigo) )
            bajaPorCodigo();

        if( click.equals(btnModificacion) )
            modificar();
    }

    /**
     * Limpia los campos de texto editables.
     */
    private void limpiarCampos() {
        etCodigo.setText("");
        etPrecio.setText("");
        etDescripcion.setText("");
    }

    private void mensajeToast(@NonNull String mensaje){
        Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
    }

    /**
     * Comprueba que todos los campos pasados por parametro hayan
     * sido completados por el usuario
     * @param listaCampos lista de campos que se quiere comprobar que no esten vacios
     * @return boolean
     */

    private boolean comprobarCampos(@NonNull List<EditText> listaCampos){

        boolean camposCompletos = true;
        for(EditText campo : listaCampos){
            String contenido = campo.getText().toString();
            if ( contenido.equals("") ){
                camposCompletos = false;
            }
        }
        return  camposCompletos;
    }
}
