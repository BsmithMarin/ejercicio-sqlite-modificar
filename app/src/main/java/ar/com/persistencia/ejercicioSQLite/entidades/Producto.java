package ar.com.persistencia.ejercicioSQLite.entidades;

import android.content.ContentValues;

import java.util.Objects;

public class Producto {

    private String descripcion;
    private int condigo;
    private double precio;

    public Producto(int condigo, String descripcion, double precio){
        setCondigo(condigo);
        setDescripcion(descripcion);
        setPrecio(precio);
    }

    public ContentValues toContentValues(){

        ContentValues values = new ContentValues();
        values.put(ContratoProducto.ProductoEntry.COLUMN_NAME_CODIGO,getCondigo());
        values.put(ContratoProducto.ProductoEntry.COLUMN_NAME_DESCRIPCION,getDescripcion());
        values.put(ContratoProducto.ProductoEntry.COLUMN_NAME_PRECIO,getPrecio());
        return values;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCondigo() {
        return condigo;
    }

    public void setCondigo(int condigo) {
        this.condigo = condigo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return condigo == producto.condigo && Double.compare(producto.precio, precio) == 0 && Objects.equals(descripcion, producto.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descripcion, condigo, precio);
    }

    @Override
    public String toString() {
        return "Producto{" +
                "descripcion='" + descripcion + '\'' +
                ", condigo=" + condigo +
                ", precio=" + precio +
                '}';
    }
}

