package ar.com.persistencia.ejercicioSQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ar.com.persistencia.ejercicioSQLite.entidades.ContratoProducto;


public class AdminSQLiteOpenHelper extends SQLiteOpenHelper{

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + ContratoProducto.ProductoEntry.TABLE_NAME + " (" +
                    ContratoProducto.ProductoEntry._ID + " INTEGER PRIMARY KEY," +
                    ContratoProducto.ProductoEntry.COLUMN_NAME_CODIGO + " INTEGER NOT NULL," +
                    ContratoProducto.ProductoEntry.COLUMN_NAME_PRECIO+" REAL NOT NULL,"+
                    ContratoProducto.ProductoEntry.COLUMN_NAME_DESCRIPCION + " TEXT NOT NULL,"+
                    "UNIQUE ("+ContratoProducto.ProductoEntry.COLUMN_NAME_CODIGO+")," +
                    "UNIQUE ("+ContratoProducto.ProductoEntry.COLUMN_NAME_DESCRIPCION+"))";

    static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "Producto.db";

    public AdminSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
