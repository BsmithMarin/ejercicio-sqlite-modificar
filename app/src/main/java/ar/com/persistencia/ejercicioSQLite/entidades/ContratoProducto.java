package ar.com.persistencia.ejercicioSQLite.entidades;

import android.provider.BaseColumns;

public class ContratoProducto {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private ContratoProducto() {}

    /* Inner class that defines the table contents */
    public static class ProductoEntry implements BaseColumns {
        public static final String TABLE_NAME = "producto";
        public static final String COLUMN_NAME_CODIGO = "codigo";
        public static final String COLUMN_NAME_PRECIO = "precio";
        public static final String COLUMN_NAME_DESCRIPCION = "descripcion";
    }

}
